import 'package:flutter/material.dart';
import "conversation_page.dart" show ConversationPage;
import '../constants.dart' show Constants;
import '../constants.dart' show AppColors;

enum ActionItems {
  GROUP_CHAT,
  ADD_FRIEND,
  QR_SCN,
  PAYMENT,
  HELP,
}

class NavigationIconView {
  final String _title;
  final IconData _icon;
  final IconData _activeIcon;
  final BottomNavigationBarItem item;

  NavigationIconView(
      {Key key, String title, IconData icon, IconData activeIcon})
      : _title = title,
        _icon = icon,
        _activeIcon = activeIcon,
        item = new BottomNavigationBarItem(
            icon: Icon(
              icon,
              color: Color(AppColors.TabIconNormal),
            ),
            activeIcon: Icon(
              activeIcon,
              color: Color(AppColors.TabIconActive),
            ),
            title: Text(
              title,
            ),
            backgroundColor: Colors.white);
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  List<NavigationIconView> _navigationViews;
  PageController _pageController;
  List<Widget> _pages;

  @override
  void initState() {
    super.initState();
    _navigationViews = [
      NavigationIconView(
        title: '微信',
        icon: IconData(
          0xe608,
          fontFamily: Constants.IconFontFamily,
        ),
        activeIcon: IconData(
          0xe603,
          fontFamily: Constants.IconFontFamily,
        ),
      ),
      NavigationIconView(
        title: '通讯录',
        icon: IconData(
          0xe601,
          fontFamily: Constants.IconFontFamily,
        ),
        activeIcon: IconData(
          0xe656,
          fontFamily: Constants.IconFontFamily,
        ),
      ),
      NavigationIconView(
        title: '发现',
        icon: IconData(
          0xe600,
          fontFamily: Constants.IconFontFamily,
        ),
        activeIcon: IconData(
          0xe671,
          fontFamily: Constants.IconFontFamily,
        ),
      ),
      NavigationIconView(
        title: '我',
        icon: IconData(
          0xe6c0,
          fontFamily: Constants.IconFontFamily,
        ),
        activeIcon: IconData(
          0xe626,
          fontFamily: Constants.IconFontFamily,
        ),
      ),
    ];
    _pageController = PageController(initialPage: _currentIndex);
    _pages = [
      ConversationPage(),
      Container(
        color: Colors.pink,
      ),
      Container(
        color: Colors.blue,
      ),
      Container(
        color: Colors.orange,
      ),
    ];
  }

  _buildPopupMunuItem(int iconName, String title) {
    return Row(
      children: <Widget>[
        Icon(
          IconData(
            iconName,
            fontFamily: Constants.IconFontFamily,
          ),
          size: 22.0,
          color: const Color(AppColors.AppBarPopupMenuColor),
        ),
        Container(
          padding: EdgeInsets.only(right: 12.0),
        ),
        Text(
          title,
          style: TextStyle(
            color: const Color(AppColors.AppBarPopupMenuColor),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final BottomNavigationBar botNavBar = BottomNavigationBar(
      items: _navigationViews
          .map<BottomNavigationBarItem>((NavigationIconView view) => view.item)
          .toList(),
      currentIndex: _currentIndex,
      fixedColor: const Color(AppColors.TabIconActive),
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentIndex = index;
          _pageController.animateToPage(_currentIndex,
              duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
        });
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: new Text("wechat"),
        elevation: 0.0, // 无阴影
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              size: 22,
            ),
            onPressed: () {
              print('点击了搜索');
            },
          ),
          Container(
            padding: EdgeInsets.only(right: 10.0),
          ),
          PopupMenuButton(
            itemBuilder: (BuildContext context) {
              return <PopupMenuItem<ActionItems>>[
                PopupMenuItem(
                  child: _buildPopupMunuItem(0xe69e, "发起群聊"),
                  value: ActionItems.GROUP_CHAT,
                ),
                PopupMenuItem(
                  child: _buildPopupMunuItem(0xe638, "添加朋友"),
                  value: ActionItems.ADD_FRIEND,
                ),
                PopupMenuItem(
                  child: _buildPopupMunuItem(0xe61b, "扫一扫"),
                  value: ActionItems.QR_SCN,
                ),
                PopupMenuItem(
                  child: _buildPopupMunuItem(0xe62a, "收付款"),
                  value: ActionItems.PAYMENT,
                ),
                PopupMenuItem(
                  child: _buildPopupMunuItem(0xe63d, "帮助与反馈"),
                  value: ActionItems.HELP,
                ),
              ];
            }, // 设置下拉显示列表具体内容
            icon: Icon(
              Icons.add,
              size: 22,
            ), // 设置btn显示的图标
            onSelected: (ActionItems selected) {
              print("点击的是$selected");
            }, // 下拉改变的事件
          ),
          Container(
            padding: EdgeInsets.only(right: 6.0),
          ), // 设置边距
        ],
      ),
      body: PageView.builder(
        itemBuilder: (BuildContext context, int index) {
          return _pages[index];
        }, // page内容
        controller: _pageController, // 控制page跳转等
        itemCount: _pages.length, // page的个数
        onPageChanged: (int index) {
          setState(() {
            _currentIndex = index;
          });
        }, // page切换事件，传入参数第几个页面index
      ),
      bottomNavigationBar: botNavBar, // 底部按钮
    );
  }
}
