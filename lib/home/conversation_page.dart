import 'package:flutter/material.dart';

import '../constants.dart';
import '../model/conversation.dart' show mockConversations, Conversation;

class _ConversationItem extends StatelessWidget {
  const _ConversationItem(this.conversation, {Key key}) : super(key: key);

  final Conversation conversation;

  @override
  Widget build(BuildContext context) {
    // 头像
    Widget avatar;
    if (conversation.isAvatarFromNet()) {
      avatar = Image.network(
        conversation.avatar,
        width: Constants.ConversationAvatarSize,
        height: Constants.ConversationAvatarSize,
      );
    } else {
      avatar = Image.asset(
        conversation.avatar,
        width: Constants.ConversationAvatarSize,
        height: Constants.ConversationAvatarSize,
      );
    }

    // 头像、小圆点
    Widget avatarContainer;
    if (conversation.unreadMsgCount > 0) {
      // 未读小圆点
      Widget unreadMsgCountText = Container(
        width: Constants.UnreadMsgNotifyDotSize,
        height: Constants.UnreadMsgNotifyDotSize,
        alignment: Alignment.center,
        // 居中
        decoration: BoxDecoration(
          borderRadius:
              BorderRadius.circular(Constants.UnreadMsgNotifyDotSize / 2.0),
          color: Color(AppColors.NotifyDotBg),
        ),
        // 设置圆角
        child: Text(
          conversation.unreadMsgCount.toString(),
          style: AppStyle.UnreadMsgCountDotStyle,
        ),
      );

      // Stack控件，先放的在下面，一层一层从左上角叠加
      avatarContainer = Stack(
        overflow: Overflow.visible, //超过边界可见性
        children: <Widget>[
          avatar,
          Positioned(
            right: -Constants.UnreadMsgNotifyDotSize / 3.0,
            top: -Constants.UnreadMsgNotifyDotSize / 3.0,
            child: unreadMsgCountText,
          ), // 绝对偏移量
        ],
      );
    } else {
      avatarContainer = avatar;
    }

    //

    // 右边显示,勿扰模式图标,时间
    var _rightArea = <Widget>[
      Text(
        conversation.updateAt,
        style: AppStyle.DescStyle,
      ),
      SizedBox(
        height: 10.0,
      ), // 用于边距间隔
    ];
    if (conversation.isMute) {
      _rightArea.add(Icon(
        IconData(
          0xe755,
          fontFamily: Constants.IconFontFamily,
        ),
        size: Constants.ConversationMuteIconSize,
        color: Color(AppColors.ConversationMuteIconColor),
      ));
    } else {
      _rightArea.add(Icon(
        IconData(
          0xe755,
          fontFamily: Constants.IconFontFamily,
        ),
        size: Constants.ConversationMuteIconSize,
        color: Colors.transparent,
      ));
    }
    return Container(
      padding: const EdgeInsets.all(10), // padding
      decoration: BoxDecoration(
        color: Color(AppColors.ConversationItemBg),
        border: Border(
          bottom: BorderSide(
            color: Color(AppColors.DividerColor),
            width: Constants.DividerWidth,
          ),
        ),
      ), // 设置每行底部border，分隔符
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
//        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          avatarContainer,
          Container(
            width: 20.0,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  conversation.title,
                  style: AppStyle.TitleStyle,
                ),
                Text(
                  conversation.des,
                  style: AppStyle.DescStyle,
                ),
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: _rightArea,
          ),
        ],
      ),
    );
  }
}

class _DeviceInfoItem extends StatelessWidget {
  const _DeviceInfoItem(this.device) : assert(device != null);

  final Device device;

  String get deviceName {
    return device == Device.MAC ? "Mac" : "Windows";
  }

  int get iconName {
    return device == Device.MAC ? 0xe640 : 0xe75e;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:
          EdgeInsets.only(left: 25.0, right: 10.0, top: 10.0, bottom: 10.0),
      color: Color(AppColors.DeviceBgColor),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            IconData(
              iconName,
              fontFamily: Constants.IconFontFamily,
            ),
            color: Color(AppColors.DeviceIconColor),
          ),
          SizedBox(
            width: 15.0,
          ),
          Text(
            '$deviceName 微信已登录',
            style: TextStyle(color: Color(AppColors.DeviceTextColor)),
          )
        ],
      ),
    );
  }
}

class ConversationPage extends StatefulWidget {
  @override
  createState() => new ConversationPageState();
}

class ConversationPageState extends State<ConversationPage> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return _DeviceInfoItem(Device.WIN);
        }
        return _ConversationItem(mockConversations[index - 1]);
      },
      itemCount: mockConversations.length + 1,
    );
  }
}
