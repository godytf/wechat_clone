import 'package:flutter/material.dart';

class AppColors {
  static const AppBarColor = 0xff303030;
  static const TabIconNormal = 0xff999999;
  static const TabIconActive = 0xff46c11b;
  static const AppBarPopupMenuColor = 0xffffffff;
  static const TitleTextColor = 0xff353535;
  static const ConversationItemBg = 0xffffffff;
  static const DescTextColor = 0xff9e9e9e;
  static const DividerColor = 0xffd9d9d9;
  static const NotifyDotBg = 0xffff3e3e;
  static const NotifyDotText = 0xffffffff;
  static const ConversationMuteIconColor = 0xffd8d8d8;
  static const DeviceBgColor = 0xfff5f5f5;
  static const DeviceIconColor = 0xff606062;
  static const DeviceTextColor = 0xff606062;


}

class AppStyle {
  static const TitleStyle = TextStyle(
    fontSize: 14.0,
    color: Color(AppColors.TitleTextColor),
  );
  static const DescStyle = TextStyle(
    fontSize: 11.0,
    color: Color(AppColors.DescTextColor),
  );
  static const UnreadMsgCountDotStyle = TextStyle(
    fontSize: 11.0,
    color: Color(AppColors.NotifyDotText),
  );
}

class Constants {
  static const IconFontFamily = "appIconFont";
  static const ConversationAvatarSize = 48.0;
  static const DividerWidth = 1.0;
  static const UnreadMsgNotifyDotSize = 20.0;
  static const ConversationMuteIconSize = 18.0;

}

enum Device{
  MAC,WIN
}